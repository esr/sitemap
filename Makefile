# Makefile for the sitemap distribution
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

MANDIR=/usr/share/man/man1
BINDIR=/usr/bin

all: sitemap-$(VERS).tar.gz

install: sitemap.1
	cp sitemap $(BINDIR)
	cp sitemap.1 $(MANDIR)/sitemap.1

sitemap.1: sitemap.xml
	xmlto man sitemap.xml

sitemap.html: sitemap.xml
	xmlto html-nochunks sitemap.xml

pylint:
	@pylint --score=n sitemap

SOURCES = README COPYING NEWS control sitemap sitemap.xml sitemap.1 Makefile sitemaprc

sitemap-$(VERS).tar.gz: $(SOURCES)
	mkdir sitemap-$(VERS)
	cp $(SOURCES) sitemap-$(VERS)
	tar -czf sitemap-$(VERS).tar.gz sitemap-$(VERS)
	rm -fr sitemap-$(VERS)
	ls -l sitemap-$(VERS).tar.gz

dist: sitemap-$(VERS).tar.gz

clean:
	rm -fr sitemap.html sitemap.1 sitemap-$(VERS).tar.gz

release: sitemap-$(VERS).tar.gz sitemap.html
	shipper version=$(VERS) | sh -e -x

refresh: sitemap.html
	shipper -N -w version=$(VERS) | sh -e -x
